package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/fatih/color"
	"github.com/shomali11/util/xhashes"
)

func convert(b []byte) string {
	s := make([]string, len(b))
	for i := range b {
		s[i] = strconv.Itoa(int(b[i]))
	}
	return strings.Join(s, ",")
}

func main() {
	cyan := color.Cyan
	red := color.Red

	s := os.Args[1]
	cyan(fmt.Sprintf("Checking if %s has been PWND\n", s))

	hash := strings.ToUpper(xhashes.SHA1(s))
	prefix := hash[:5]
	sufix := hash[5:]

	cyan(fmt.Sprintf("Checking hash: %s with %s\n", hash, prefix))

	resp, err := http.Get(fmt.Sprintf("https://api.pwnedpasswords.com/range/%s", prefix))
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	lines := strings.Split(string(body), "\r\n")

	for _, line := range lines {
		if strings.Contains(strings.ToUpper(line), sufix) {
			red(fmt.Sprintf("You PWND: %s\n", line))
		}
	}

}
